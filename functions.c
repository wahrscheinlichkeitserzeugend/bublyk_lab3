#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define true 1

char PRIMITIVE[] = "20000000000000000000000000000000000000000407";

uint64_t power (uint64_t base, int i) {
	
	uint64_t res = 1;
	
	if (i == 0) return res;
	else 
	{
		for (int j = 0; j < i; j++) 
			res *= base;
	}

	return res;
}

/*allocates memory for feild member and sets all bits to zero*/
uint64_t *pol_zero () {
	
	return calloc(3, sizeof(uint64_t));
}

/*allocates memory for feild member and initializes it as neutral by multiplication*/
uint64_t *pol_one () {
	
	uint64_t *t = pol_zero();
	t[0] = 1;

	return t;
}

/*returns xor of a and b*/
uint64_t *pol_xor (uint64_t *a, uint64_t *b) {
	
	uint64_t *tmp = pol_zero();
	
	for (int i = 0; i < 3; i++)
		tmp[i] = a[i] ^ b[i];

	return tmp;
}

/*returns a & b*/
uint64_t *pol_and (uint64_t *a, uint64_t *b) {

	uint64_t *tmp = pol_zero();

	for (int i = 0; i < 3; i++)
	 tmp[i] = a[i] & b[i];
	
	return tmp;
}

/*returns 0 if argument is bigger than zero, 1 if equals*/
int is_zero (uint64_t *pol) {

	int i = 0;

	for ( ; i < 3; i++) 
		if (pol[i]) return 0;
	
	if (i == 3) return 1;
	else return -1;
}

/*returns value of bit in polynomial pol in posotion num*/
int bit_at (uint64_t *pol, int num) {
	
	return (pol[num >> 6] >> (num % 64)) & 1; 
}

/*displays argument*/
void pol_to_str (uint64_t *pol) {

	//uncomment following if need to display 
	//polynomial in binary form

	/*putchar('[');

	for (int i = 173; i >= 0; i--)
		printf("%d", bit_at(pol, i));

	puts("]");*/

	printf("\t\t[");

	for (int i = 2; i >= 0; i--) {
		printf("%lx ", pol[i]);

	}
	puts("\b]");
}

/*converts string of hex format to polynomial*/
uint64_t *to_pol (char *str) {

	uint64_t *pol = pol_zero();
	int j = 0, q = -1;

	for (int i = 43; i >= 0; i--, j++) {

		if (j % 16 == 0) {
			j = 0;
			q++;
			pol[q] = 0;
		}

		if (str[i] >= 48 && str[i] <= 57) {
			pol[q] += (str[i] - 48) * power(16, j);
		}
		else if (str[i] >= 65 && str[i] <= 70) {
			pol[q] += (str[i] - 55) * power(16, j);
		}
		else {
			printf("%d\n", str[i]);
			puts("wrong format\n");
			return NULL;
		}	
	}

	return pol;
}

/*returns sum of given polynomials*/
uint64_t *pol_add (uint64_t *a, uint64_t *b) {
	
	return pol_xor(a, b);
}

/*copies contents of one polynomial to another*/
void pol_cpy (uint64_t *src, uint64_t *dest) {

	for (int i = 0; i < 3; i++)
		dest[i] = src[i];

}

/*shifts polynomial one position up*/
void sh_up (uint64_t *a) {
	
	const uint64_t msk = 1UL << 63;
	uint64_t t = 0,
					 q = 0;

	for (int i = 0; i < 3; i++) {
		q = (msk & a[i]) >> 63;
		a[i] <<= 1;
		a[i] |= t;
		t = q;		
	}
}

/*shifts polynomial one position down*/
void sh_down (uint64_t *a) {

	uint64_t t = 0,
					 q = 0;

	for (int i = 2; i >= 0; i--) {
		q = (1UL & a[i]) << 63;
		a[i] >>= 1;
		a[i] |= t;
		t = q;
	}
}

/*multiplies two polynomials in their field*/
uint64_t *pol_mul (uint64_t *a, uint64_t *b) {
	
	uint64_t *t = pol_zero(),
					 *s = pol_zero(),
					 *u = pol_zero(),
					 h = 1UL << 44;

	pol_cpy(b, u);

	while (is_zero(u) == 0) {
		
		if (u[0] & 1UL)		t = pol_xor(t, a);

		sh_down(u);
		s[2] = h & a[2];
		sh_up(a);

		if (s[2])		a = pol_xor(a, to_pol(PRIMITIVE));
	}

	return t;
}

/*returns squared argument*/
uint64_t *pol_sq (uint64_t *a) {

	uint64_t *tmp = pol_zero();
	pol_cpy(a, tmp);

	return pol_mul(a, tmp);
}

/*returns mask where true bit marks position of highest true bit of argument*/
uint64_t *highest_one (uint64_t *a) {

	uint64_t *t = pol_zero();
	int i;
	
	for (i = 173; i >= 0; i--)
		if (bit_at(a, i) == 1) break;
	
	t[i / 64] = 1UL << i % 64;

	return t;
}

/*returns (base ^ exp) mod (primitive)*/
uint64_t *pol_exp (uint64_t *base, uint64_t *exp) {
	
	uint64_t *s = pol_zero(),
					 *b = highest_one(exp);
	
	pol_cpy(base, s);

	while (true) { 

		if ((b[2] == 0) && (b[1] == 0) && (b[0] == 1)) break;

		sh_down(b);
		s = pol_sq(s);

		if (is_zero(pol_and(exp, b)) == 0) {
			s = pol_mul(s, base);
		}	
	}

	return s;
}

/*returns trace of field member*/
uint64_t *trace (uint64_t *a) {
	
	uint64_t *t = pol_zero();
	pol_cpy(a, t);

	for (int i = 1; i < 173; i++) {
		a = pol_sq(a);
		t = pol_add(t, a);
	}

	return t;
}

/*reurns iverted field member*/
uint64_t *inv (uint64_t *a) {

	uint64_t *s = pol_zero();

	pol_cpy(a, s);

	for (int i = 0; i <= 170; i++) {
		
		a = pol_sq(a);
		s = pol_mul(s, a);
	}

	return pol_sq(s);
}