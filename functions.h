#include <stdint.h>

uint64_t power (uint64_t, int);

uint64_t *pol_add (uint64_t *, uint64_t *);

uint64_t *pol_zero ();

uint64_t *pol_one ();

uint64_t *pol_xor (uint64_t *, uint64_t *);

uint64_t *pol_and (uint64_t *, uint64_t *);

void sh_up (uint64_t *);

void sh_down (uint64_t *);

int bit_at (uint64_t *, int);

int is_zero (uint64_t *);

void pol_to_str (uint64_t *);

void pol_cpy (uint64_t *, uint64_t *);

uint64_t *pol_mul (uint64_t *, uint64_t *);

uint64_t *pol_sq (uint64_t *);

uint64_t *highest_one (uint64_t *);

uint64_t *pol_exp (uint64_t *, uint64_t *);

uint64_t *inv (uint64_t *);


uint64_t *to_pol (char *);

uint64_t *trace (uint64_t *);