#include <stdio.h>
#include "functions.h"

int main(void) {
  printf("Hello World\n");

	char astr[] = "158F00BDA2162C2C363B3761CD0BFFAE6039C4297D03";
	char bstr[] = "0B51FBDDD8197DBBC9E8B1A19E6B7CAF8409F5F2A26D";
	char nstr[] = "15177C6EAAE6B14398EF683EB812C598256C7E264430";
	char pstr[] = "045854C570DDAC0973BEE65B5AB2E54F39DD532324F4";

	puts("Polynomial A:");
	pol_to_str(to_pol(astr));
	puts("Polynomial B:");
	pol_to_str(to_pol(bstr));
	puts("Polynomial N:");
	pol_to_str(to_pol(nstr));
	
	puts("\nAddition:");
	pol_to_str(pol_add(to_pol(astr), to_pol(bstr)));
	puts("\nMultiplication:");
	pol_to_str(pol_mul(to_pol(astr), to_pol(bstr)));
	puts("\nSquare:");
	pol_to_str(pol_sq(to_pol(astr)));
	puts("\nExponentiation:");
	pol_to_str(pol_exp(to_pol(astr), to_pol(nstr)));
	puts("\nTrace:");
	pol_to_str(trace(to_pol(pstr)));
	puts("\nInvertion:");
	pol_to_str(inv(to_pol(astr)));

  return 0;
}